import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class FizzBuzzTest {

	/**
	 * La chiamata al metodo fizzbuzzSequence con parametro intero 1,
	 * deve restituire la stringa "1".
	 */
	FizzBuzz prova;
	@Before
	public void creaFizz(){
		prova = new FizzBuzz();
	}
	
	@Test
	public void fizzbuzzSequence1() {
		String risultato = prova.fizzbuzzSequence(1);
		assertEquals("1", risultato);
	}
	
	@Test
	public void fizzbuzzSequence2(){
		String risultato =prova.fizzbuzzSequence(2);
		assertEquals("1 2", risultato);
	}
	
	@Test
	public void fizzbuzzSequence3(){
		String risultato = prova.fizzbuzzSequence(3);
		assertTrue(risultato.endsWith("2 fizzfizz"));
	}
	
	@Test
	public void fizzbuzzSequence4(){
		String risultato = prova.fizzbuzzSequence(5);
		assertTrue(risultato.endsWith("4 buzzbuzz"));
	}
	
	@Test
	public void fizzbuzzSequence5(){
		String risultato = prova.fizzbuzzSequence(6);
		assertTrue(risultato.endsWith("fizz"));
	}

	@Test
	public void fizzbuzzSequence6(){
		String risultato = prova.fizzbuzzSequence(10);
		assertTrue(risultato.endsWith("fizz buzz"));
	}
	
	@Test
	public void fizzbuzzSequence7(){
		String risultato = prova.fizzbuzzSequence(15);
		assertTrue(risultato.endsWith("fizzbuzz"));
	}
	
	@Test
	public void fizzbuzzSequence8(){
		String risultato = prova.fizzbuzzSequence(100);
		assertTrue(risultato.endsWith("woof fizz buzz"));
	}

	@Test 
	public void fizzbuzzSequence9(){
		try{
		String risultato = prova.fizzbuzzSequence(-1);
		}catch (NumberFormatException e){
			String messaggio = e.getMessage();
			assertEquals("non accetto numeri negativi", messaggio);
		}
	}
	
	@Test
	public void fizzbuzzSequence10(){
		String risultato = prova.fizzbuzzSequence(7);
		assertTrue(risultato.endsWith("woof"));		
	}

	@Test
	public void fizzbuzzSequence11(){
		String risultato = prova.fizzbuzzSequence(35);
		assertTrue(risultato.endsWith("fizzbuzzbuzzwoof"));		
	}
	
	@Test
	public void fizzbuzzSequence12(){
		String risultato = prova.fizzbuzzSequence(53);
		assertTrue(risultato.endsWith("fizzbuzz"));	
	}
	
}
