
public abstract class FizzBuzzWoof {

	public abstract String containsDigit(int numero);
	
	public abstract String isDivided(int numero);
	
	public abstract boolean isFizzBuzzWoof(int numero);
}
