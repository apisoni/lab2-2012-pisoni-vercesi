
public class FizzBuzz implements IfizzBuzz{

	public FizzBuzz(){
		}
	
	public String fizzbuzzSequence(int number) throws NumberFormatException {
		Woof woofator = new Woof();
		Fizz fizzator = new Fizz();
		Buzz buzzator = new Buzz();
		boolean stampaNumero = true;
		String temp = "";
		
		if(number < 0)
			throw new NumberFormatException("non accetto numeri negativi");
		
		for (int contatore=1; contatore<=number; contatore++) {
			stampaNumero=true;
			String buff = ""+contatore;
			
			
			temp+=fizzator.containsDigit(contatore)
			+buzzator.containsDigit(contatore)
			+fizzator.isDivided(contatore)
			+buzzator.isDivided(contatore)
			+woofator.isDivided(contatore);
			
			
			if(contatore%3!=0 && contatore%5!=0 && contatore%7!=0 && buzzator.isFizzBuzzWoof(contatore)==false&&fizzator.isFizzBuzzWoof(contatore)==false)
				temp = temp + contatore;
			if (contatore < number)
				temp = temp + " ";
		}
		return temp;
	}
	
}
